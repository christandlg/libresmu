# LibreSMU

A project aimed at creating an open source [Source Measure Unit](https://en.wikipedia.org/wiki/Source_measure_unit). 

Published under CERN-OHL-S and GNU GPL v3. 